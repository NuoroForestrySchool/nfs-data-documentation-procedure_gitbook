# Introduction

This text presents the 'data documentation procedure' that has been devised within NFS to enable the adoption of SQLite files as common good practice to implement the FAIR principles.

The procedure considers the most common USE CASE: data are stored in a spreadsheet.

It includes two main phases:

1. production of a structured SQLite DB
2. production of the documentatation pfd file

**Table of Contents** _generated with_ [_DocToc_](https://github.com/thlorenz/doctoc)

* [GitLab CI](./#gitlab-ci)
* [Building locally](./#building-locally)
* [GitLab User or Group Pages](./#gitlab-user-or-group-pages)
* [Did you fork this project?](./#did-you-fork-this-project)
* [Troubleshooting](./#troubleshooting)



***
